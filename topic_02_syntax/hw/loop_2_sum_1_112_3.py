"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def sum_1_112_3():
    a=1
    summ = 1
    while a < 112:
        a += 3
        summ += a
    return (summ)

if __name__=="__main__":
    sum_1_112_3()
