"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""
def check_substr(first_str, second_str):
    if first_str ==  second_str:
        return False
    if first_str == "" or second_str == "":
        return True
    elif len(first_str) < len(second_str) and first_str in second_str \
            or len(second_str) <len(first_str) and second_str in first_str:
        return True

    else:
        return False


