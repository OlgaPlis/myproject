"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
Если число меньше или равно 0, то вернуть "Must be > 0!".
"""

def count_odd_num(a):
    if type(a) != int:
        return "Must be int!"
    elif a <= 0:
        return "Must be > 0!"
    else:
        b = 0
        for num in str(a):
            if int(num) % 2 != 0:
                b += 1
        return b

