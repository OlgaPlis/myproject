"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""

def print_symbols_if (string):
    s = str(string)
    if s == "":
        print("Empty string!")
    elif len(s) >  5:
        print(s[:3]+s[-3:])
    else:
        print(len(s) * s[0] )

if __name__=="__main__":
    print_symbols_if("")
