"""
Класс Goat.

Поля:
имя: name,
возраст: age,
сколько молока дает в день: milk_per_day.

Методы:
get_sound: вернуть строку 'Бе-бе-бе',
__invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
__mul__: умножить milk_per_day на число. вернуть self
"""

class Goat:

    def __init__(self, name, age, milk_per_day):
        self.name = name
        self.age = age
        self.milk_per_day = milk_per_day

    def get_sound(self):
        return 'Бе-бе-бе'

    def __invert__(self):
        self.name = self.name[::-1].title()
        return self

    def __mul__(self, other):
        self.milk_per_day = self.milk_per_day * other
        return self


if __name__ == '__main__':
    chika1 = Goat('Луиза Поликарповна', 458, 8)
    chika2 = Goat('Аксинья Павловна', 956, 11)
    chika3 = Goat('Арина Ростиславовна', 775, 6)
    print(chika2.get_sound())

