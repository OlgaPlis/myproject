"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""
from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker


class School:

    def __init__(self, people, number):
        self.people = people
        self.number = number

    def get_avg_mark(self):
        all_marks = [x.get_avg_mark() for x in self.people if isinstance(x, Pupil)]
        return sum(all_marks) / len(all_marks)

    def get_avg_salary(self):
        all_salary = [x.salary for x in self.people if isinstance(x, Worker)]
        return sum(all_salary) / len(all_salary)

    def get_worker_count(self):
        return len([y for y in self.people if isinstance(y, Worker)])

    def get_pupil_count(self):
        return len([x for x in self.people if isinstance(x, Pupil)])

    def get_pupil_names(self):
        return [x.name for x in self.people if isinstance(x, Pupil)]

    def get_unique_worker_positions(self):
        return set([x.position for x in self.people if isinstance(x, Worker)])

    def get_max_pupil_age(self):
        return max(x.age for x in self.people if isinstance(x, Pupil))

    def get_min_worker_salary(self):
        return min([d.salary for d in self.people if isinstance(d, Worker)])

    def get_min_salary_worker_names(self):
        return [x.name for x in self.people if isinstance(x, Worker) if x.salary == self.get_min_worker_salary()]


