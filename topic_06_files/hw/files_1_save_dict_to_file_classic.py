"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic (path, my_dict):
    with open(path, 'w') as my_file:
        my_file.write(str(my_dict))



if __name__=="__main__":
    file_path = 'ya_molodec.txt'
    save_dict_to_file_classic(file_path,{1: 'тратата',2:"тратата",3: "мы везём с собой кота"})