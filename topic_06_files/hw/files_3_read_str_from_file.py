"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""
import json


def read_str_from_file(path: str):
    with open(path, 'r') as a:
        for line in a:
            print(line)


if __name__ == '__main__':
    read_str_from_file('ya_molodec.txt')
