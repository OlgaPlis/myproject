"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""
import pickle


def save_dict_to_file_pickle(my_path, my_dict):
    with open(my_path, 'wb') as pickle_file:
        pickle.dump(my_dict, pickle_file)


if __name__ == '__main__':
    ex_dict = {1: 'I can not',
               2: 'think of',
               3: 'a dictionary'
               }
    file_name = 'ya_krasavchik.pkl'

    save_dict_to_file_pickle(file_name, ex_dict)

    with open(file_name, 'rb') as a:
        dict_load = pickle.load(a)

    print(f'type: {type(dict_load)}')
    print(f'equal:{ex_dict == dict_load}')
