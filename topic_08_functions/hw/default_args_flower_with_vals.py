"""
Функция flower_with_default_vals.

Принимает 3 аргумента:
цветок (по умолчанию "ромашка"),
цвет (по умолчанию "белый"),
цена (по умолчанию 10.25).

Функция flower_with_default_vals выводит строку в формате
"Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

(* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
В функции main вызвать функцию flower_with_default_vals различными способами
(перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

(* Использовать именованные аргументы).
"""


def flower_with_default_vals(flow="Ромашка", col="Белый", pr=10.25):
    if type(flow) != str or type(col) != str:
        print('Must be str!')
    if 0 < pr > 10000:
        print("Incorrect price!")
    else:
        print(f'Цветок: {flow} | Цвет:{col} | Цена: {pr}')


if __name__=="__main__":
    flower_with_default_vals(flow="Роза",col=55,pr=1220)
    flower_with_default_vals(col="Розовый",pr=110)
    flower_with_default_vals(flow="Тюльпан",pr=200)
    flower_with_default_vals(flow="Кактус",col="Красный")
    flower_with_default_vals("Лилия","Черный",229)
