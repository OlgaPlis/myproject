from project.flirtbuttle.my_heroes import MyHeroes

heroes_weaknesses = {
    MyHeroes.INTELLIGENT: (MyHeroes.ATHLETIC,
                           MyHeroes.CLEVER),

    MyHeroes.TALENTED: (MyHeroes.FUNNY,
                        MyHeroes.STRONG),

    MyHeroes.CLEVER: (MyHeroes.TALENTED,
                      MyHeroes.RICH),

    MyHeroes.BEAUTIFUL: (MyHeroes.RICH,
                         MyHeroes.ATHLETIC),

    MyHeroes.ATHLETIC: (MyHeroes.FUNNY,
                        MyHeroes.BEAUTIFUL),

    MyHeroes.FUNNY: (MyHeroes.STRONG,
                     MyHeroes.INTELLIGENT),

    MyHeroes.STRONG: (MyHeroes.TALENTED,
                      MyHeroes.CLEVER),

    MyHeroes.RICH: (MyHeroes.BEAUTIFUL,
                    MyHeroes.INTELLIGENT)
}
