from enum import Enum, auto


class GameResult(Enum):
    BROKEN_HEARTS = auto()
    LONELY_EVENINGS = auto()
    NEW_FRIENDS = auto()


