import json


import telebot
from telebot import types
from telebot.types import Message

from project.flirtbuttle.actions import ACTIONS
from project.flirtbuttle.your_result import GameResult
from project.flirtbuttle.hero import HERO
from project.flirtbuttle.hero_npc import HeroNPC
from project.flirtbuttle.my_heroes_groups import MyHeroes_Groups
from project.flirtbuttle.Heroes_state import HeroesState
from project.flirtbuttle.my_heroes import MyHeroes

with open("./bot_token.txt", 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

state = {}

statistics = {}

stat_file = "game_stat.json"

actions_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                             one_time_keyboard=True,
                                             row_width=len(ACTIONS))

actions_keyboard.row(*[types.KeyboardButton(actions.name) for actions in ACTIONS])


def update_save_stat(chat_id, result: GameResult):
    print('Statistics update...', end='')
    global statistics

    chat_id = str(chat_id)

    if statistics.get(chat_id, None) is None:
        statistics[chat_id] = {}

    if result == GameResult.BROKEN_HEARTS:
        statistics[chat_id]['BROKEN_HEARTS'] = statistics[chat_id].get('BROKEN_HEARTS', 0) + 1
    elif result == GameResult.LONELY_EVENINGS:
        statistics[chat_id]['LONELY_EVENINGS'] = statistics[chat_id].get('LONELY_EVENINGS', 0) + 1
    elif result == GameResult.NEW_FRIENDS:
        statistics[chat_id]['NEW_FRIENDS'] = statistics[chat_id].get('NEW_FRIENDS', 0) + 1
    else:
        print(f"There is no result {result}")

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print("Update completed")


def load_stat():
    print('Download statistics...', end='')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('Loading is complete!')
    except FileNotFoundError:
        statistics = {}
        print('File not found!')


@bot.message_handler(commands=["help", "info"])
def help_command(message):
    bot.send_message(message.chat.id,
                     "Настало время первого свидания\n/start начать свидание \n/stat потешить самолюбие(или нет)")


@bot.message_handler(commands=["stat"])
def stat(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = "Еще не было свиданий"
    else:
        user_stat = "Твои результаты:"
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f"\n{res}: {num}"

    bot.send_message(message.chat.id,text=user_stat)


@bot.message_handler(commands=["start"])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да", "Нет")

    bot.send_message(message.from_user.id,
                     text="Время разбивать сердца, начнем?",
                     reply_markup=yes_no_keyboard)
    bot.register_next_step_handler(message, start_question_handler)


def start_question_handler(message):
    if message.text.lower() == 'да':
        bot.send_message(message.from_user.id,
                         'Тогда включай свое обаяние на полную катушку, крошка!')
        create_npc(message)
        ask_user_about_hero_type(message)

    elif message.text.lower() == 'нет':
        bot.send_message(message.from_user.id,
                         'Если тебе нужно прихорошиться, я подожду.')
    else:
        bot.send_message(message.from_user.id,
                         'Я не понимаю тебя!')


def create_npc(message):
    print(f"Начало создания объекта NPC для chat id = {message.chat.id}")
    global state
    hero_npc = HeroNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['npc_hero'] = hero_npc

    npc_stickers_filename = MyHeroes_Groups[hero_npc.heroes_type][hero_npc.name]
    bot.send_message(message.chat.id, 'Впереди свидание с:')
    with open(f"../stickers/{npc_stickers_filename}", 'rb') as file:
        bot.send_sticker(message.chat.id, file)
        bot.send_message(message.chat.id, hero_npc)
    print(f"Завершено создание объекта  NPC для chat id = {message.chat.id}")


def ask_user_about_hero_type(message):
    markup = types.InlineKeyboardMarkup()

    for heroes_type in MyHeroes:
        markup.add(types.InlineKeyboardButton(text=heroes_type.name,
                                              callback_data=f"heroes_type_{heroes_type.value}"))

    bot.send_message(message.chat.id, "За кого ты будешь играть?:", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "heroes_type_" in call.data)
def heroes_type_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 3 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        heroes_type_id = int(call_data_split[2])

        bot.send_message(call.message.chat.id, "Выбери за кого ты будешь играть:")

        ask_user_about_MyHeroes_Groups(heroes_type_id, call.message)


def ask_user_about_MyHeroes_Groups(heroes_type_id, message):
    heroes_type = MyHeroes(heroes_type_id)
    MyHeroes_Groups_dict = MyHeroes_Groups.get(heroes_type, {})

    for hero_name, hero_stick in MyHeroes_Groups_dict.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=hero_name,
                                              callback_data=f"hero_name_{heroes_type_id}_{hero_name}"))
        with open(f"../stickers/{hero_stick}", 'rb') as file:
            bot.send_sticker(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "hero_name_" in call.data)
def hero_name_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        heroes_type_id, hero_name = int(call_data_split[2]), call_data_split[3]

        create_user_hero(call.message, heroes_type_id, hero_name)

        bot.send_message(call.message.chat.id, "Игра началась!")

        game_next_step(call.message)


def create_user_hero(message, heroes_type_id, hero_name):
    print(f"Начало создания объекта HERO для chat id = {message.chat.id}")
    global state
    user_hero = HERO(name=hero_name,
                     heroes_type=MyHeroes(heroes_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_hero'] = user_hero

    stickers_filename = MyHeroes_Groups[user_hero.heroes_type][user_hero.name]
    bot.send_message(message.chat.id, 'Это ты!:')
    with open(f"../stickers/{stickers_filename}", 'rb') as file:
        bot.send_sticker(message.chat.id, file)
        bot.send_message(message.chat.id, user_hero)

    print(f"Завершено создание объекта HERO для chat id = {message.chat.id}")


def game_next_step(message: Message):
    bot.send_message(message.chat.id,
                     "Выбери чего ты хочешь избежать?:",
                     reply_markup=actions_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not ACTIONS.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        bot.send_message(message.chat.id,
                         "Чем хочешь запомниться?:",
                         reply_markup=actions_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_actions=message.text)


def reply_attack(message: Message, defend_actions: str):
    if not ACTIONS.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        attack_actions = message.text

        global state
        user_hero = state[message.chat.id]['user_hero']
        hero_npc = state[message.chat.id]['npc_hero']

        user_hero.next_step_points(next_attack=ACTIONS[attack_actions],
                                   next_defence=ACTIONS[defend_actions])
        hero_npc.next_step_points()
        game_step(message, user_hero, hero_npc)


def game_step(message: Message, user_hero: HERO, hero_npc: HERO):
    comment_npc = hero_npc.get_hit(opponent_attack_point=user_hero.attack_point,
                                   opponent_hit_power=user_hero.hit_power,
                                   opponent_type=user_hero.heroes_type)
    bot.send_message(message.chat.id, f"Твой противник: {comment_npc}\nHP: {hero_npc.hp}")
    comment_user = user_hero.get_hit(opponent_attack_point=hero_npc.attack_point,
                                     opponent_hit_power=hero_npc.hit_power,
                                     opponent_type=hero_npc.heroes_type)
    bot.send_message(message.chat.id, f"Твой герой: {comment_user}\nHP: {user_hero.hp}")

    if hero_npc.state == HeroesState.READY and user_hero.state == HeroesState.READY:
        bot.send_message(message.chat.id, "Продолжаем!")
        game_next_step(message)
    elif hero_npc.state == HeroesState.DEFEATED and user_hero.state == HeroesState.DEFEATED:
        bot.send_message(message.chat.id, "Ничья, вы теперь пара!")
        update_save_stat(message.chat.id, GameResult.NEW_FRIENDS)
    elif hero_npc.state == HeroesState.DEFEATED:
        bot.send_message(message.chat.id, "Игра окончена! Ты разбил мое сердце:)")
        update_save_stat(message.chat.id, GameResult.BROKEN_HEARTS)
    elif user_hero.state == HeroesState.DEFEATED:
        bot.send_message(message.chat.id, "Игра окончена!Этот вечер наполнился слезами твоего одиночества :)")
        update_save_stat(message.chat.id, GameResult.LONELY_EVENINGS)


if __name__ == '__main__':
    load_stat()

    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
