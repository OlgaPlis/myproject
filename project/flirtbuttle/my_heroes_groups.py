from project.flirtbuttle.my_heroes import MyHeroes

MyHeroes_Groups = {
    MyHeroes.INTELLIGENT: {'Diana': 'i_diana.webp',
                           'Lidia': 'i_lidia.webp',
                           'Raffael': 'i_raffael.webp',
                           'Rostislav': 'i_Rostislav.webp'},

    MyHeroes.TALENTED: {'Billy': 't_billy.webp',
                        'Elton': 'T_elton.webp',
                        'Keanu': 't_keanu.webp',
                        'Loki': 't_locky.webp',
                        'Phoebe': 't_phoebe.webp'},

    MyHeroes.CLEVER: {'Belle': 'c_belle.webp',
                      'Hermione': 'c_germiona.webp',
                      'Mirra': 'c_mirra.webp',
                      'Pasha': 'c_pasha.webp',
                      'Vault': 'c_vault.webp'},

    MyHeroes.BEAUTIFUL: {'Jessica': 'b_jessica.webp',
                         'Johny': 'b_johny.webp',
                         'Miriam': 'b_miriam.webp',
                         'Odry': 'b_odri.webp',
                         'Thor': 'b_tor.webp'},

    MyHeroes.ATHLETIC: {'Alla': 'sp_Alla.webp',
                        'Boris': 'sp_boris.webp',
                        'Grigorio': 'sp_Grigorio.webp',
                        'Mike': 'sp_Mike.webp'},

    MyHeroes.FUNNY: {'Anna': 'f_anna.webp',
                     'Gabe': 'f_gabe.webp',
                     'Hottie': 'f_hottie.webp',
                     'Jane': 'f_Jane.webp',
                     'Jenna': 'f_Jenna.webp',
                     'Nastya': 'f_nastya.webp',
                     'Petr': 'f_petr.webp'},

    MyHeroes.STRONG: {'Bianka': 's_bianka.webp',
                      'Drogo': 's_drogo.webp',
                      'Joan': 's_Joan.webp',
                      'Katty': 's_Katty.webp',
                      'Kitniss': 's_kitniss.webp',
                      'Lara': 's_Lara.webp'},

    MyHeroes.RICH: {'Brianna': 'r_Brianna.webp',
                    'Cleo': 'r_cleo.webp',
                    'Elon': 'r_elon.webp',
                    'Gekko': 'r_gekko.webp',
                    'Richard': 'r_richard.webp',
                    'Sultan': 'r_sultan.webp'}

}
