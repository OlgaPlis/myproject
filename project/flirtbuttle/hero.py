from project.flirtbuttle.my_heroes import MyHeroes
from project.flirtbuttle.heroes_weaknesses import heroes_weaknesses
from project.flirtbuttle.actions import ACTIONS
from project.flirtbuttle.Heroes_state import HeroesState


class HERO:
    def __init__(self, name: str, heroes_type: MyHeroes):
        self.name = name
        self.heroes_type = heroes_type
        self.weaknesses = heroes_weaknesses.get(heroes_type, tuple())
        self.hp = 100
        self.attack_point = None
        self.defence_point = None
        self.hit_power = 20
        self.state = HeroesState.READY

    def __str__(self):
        return f"Имя: {self.name} | Типаж: {self.heroes_type.name}\nСтепень безразличия: {self.hp}"

    def next_step_points(self,
                         next_attack: ACTIONS,
                         next_defence: ACTIONS):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self,
                opponent_attack_point: ACTIONS,
                opponent_hit_power: int,
                opponent_type: MyHeroes):
        if opponent_attack_point == ACTIONS.DANCE:
            return "Ммм...Танго танец страсти!"
        elif self.defence_point == opponent_attack_point:
            return "Становится скучно!"
        else:
            self.hp -= opponent_hit_power * (2 if opponent_type in self.weaknesses else 1)

            if self.hp <= 0:
                self.state = HeroesState.DEFEATED
                return "Мне кажется я влюбляюсь!"
            else:
                return "Ты милый. но не достаточно..."


if __name__ == '__main__':
    Test = HERO('Diana', MyHeroes.INTELLIGENT)
    print(Test)
