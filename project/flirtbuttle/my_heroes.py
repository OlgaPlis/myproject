from enum import Enum, auto


class MyHeroes(Enum):
    INTELLIGENT = auto()
    TALENTED = auto()
    CLEVER = auto()
    BEAUTIFUL = auto()
    ATHLETIC = auto()
    FUNNY = auto()
    STRONG = auto()
    RICH = auto()


    @classmethod
    def min_value(cls):
        return cls.INTELLIGENT.value

    @classmethod
    def max_value(cls):
        return cls.RICH.value
