import random

from project.flirtbuttle.hero import HERO
from project.flirtbuttle.my_heroes import MyHeroes
from project.flirtbuttle.my_heroes_groups import MyHeroes_Groups
from project.flirtbuttle.actions import ACTIONS


class HeroNPC(HERO):
    def __init__(self):
        rand_type_value = random.randint(MyHeroes.min_value(), MyHeroes.max_value())
        rand_heroes_type = MyHeroes(rand_type_value)
        rand_hero_name = random.choice(list(MyHeroes_Groups.get(rand_heroes_type, {}).keys()))

        super().__init__(rand_hero_name, rand_heroes_type)

    def next_step_points(self, **kwargs):
        attack_point = ACTIONS(random.randint(ACTIONS.min_value(), ACTIONS.max_value()))
        defence_point = ACTIONS(random.randint(ACTIONS.min_value(), ACTIONS.max_value()))
        super().next_step_points(next_attack=attack_point,next_defence=defence_point)


if __name__ == '__main__':
    test = HeroNPC()
    test.next_step_points()
    print(test)