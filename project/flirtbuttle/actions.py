from enum import Enum, auto


class ACTIONS (Enum):
    KISS = auto()
    DANCE = auto()
    TOUCH = auto()
    HUG = auto()


    @classmethod
    def min_value(cls):
        return cls.KISS.value

    @classmethod
    def max_value(cls):
        return cls.HUG.value

    @classmethod
    def has_item(cls, name: str):
        return name in cls._member_names_
