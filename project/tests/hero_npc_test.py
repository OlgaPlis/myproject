import random

from project.flirtbuttle.hero_npc import HeroNPC
from project.flirtbuttle.Heroes_state import HeroesState
from project.flirtbuttle.my_heroes import MyHeroes
from project.flirtbuttle.heroes_weaknesses import heroes_weaknesses


class TestHeroNPCClass:
    hero_name = 'Raffael'
    hero_type = MyHeroes.INTELLIGENT
    max_hp = 100

    def setup_method(self, method):
        random.seed(123)

    def test_init(self):
        hero_test = HeroNPC()

        assert hero_test.name == self.__class__.hero_name
        assert hero_test.heroes_type == self.__class__.hero_type
        assert hero_test.weaknesses == heroes_weaknesses[self.__class__.hero_type]
        assert hero_test.hp == self.__class__.max_hp
        assert hero_test.attack_point is None
        assert hero_test.defence_point is None
        assert hero_test.hit_power == 20
        assert hero_test.state == HeroesState.READY

    def test_str(self):
        hero_test = HeroNPC()
        assert str(hero_test) == f"Name: {self.__class__.hero_name} | " \
                                 f"Type: {self.__class__.hero_type.name}\n" \
                                 f"HP: {self.__class__.max_hp}"
