"""
Функция zip_car_year.

Принимает 2 аргумента: список с машинами и список с годами производства.

Возвращает список с парами значений из каждого аргумента, если один список больше другого,
то заполнить недостающие элементы строкой "???".

Подсказка: zip_longest.

Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
"""
from itertools import zip_longest


def zip_car_year(c_l: list, ye_l: list):
    if type(c_l) != list or type(ye_l) != list:
        return 'Must be list!'
    if not c_l or not ye_l:
        return 'Empty list!'
    return list(zip_longest(c_l, ye_l, fillvalue="???"))
