"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(first_dict):
    if type(first_dict) != dict:
        return 'Must be dict!'
    if len(first_dict) == 0:
        return [], [], 0, 0
    key_list = list(first_dict.keys())
    value_list = list(first_dict.values())
    hm_keys = len(set(key_list))
    hm_values = len(set(value_list))
    return key_list, value_list, hm_keys, hm_values


if __name__ == "__main__":
    print(dict_to_list({1: 6, 2: 6, 3: 9, 4: 5, 6: 1220, 8: 6}))
