"""
Функция magic_mul.

Принимает 1 аргумент: список my_list.

Возвращает список, который состоит из
[первого элемента my_list]
+ [три раза повторенных списков my_list]
+ [последнего элемента my_list].

Пример:
входной список [1,  'aa', 99]
результат [1, 1,  'aa', 99, 1,  'aa', 99, 1,  'aa', 99, 99].

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""


def magic_mul(my_list):
    if type(my_list) != list:
        return 'Must be list!'
    if len(my_list) == 0:
        return 'Empty list!'
    else:
        res_list = my_list * 3
        res_list += [my_list[-1]]
        res_list.insert(0, my_list[0])
        return res_list


if __name__ == '__main__':
    list_test = [1]
    print(magic_mul(list_test))
