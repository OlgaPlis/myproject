"""
Функция list_to_str.

Принимает 2 аргумента: список и разделитель (строка).

Возвращает (строку полученную разделением элементов списка разделителем,
количество разделителей в получившейся строке в квадрате).

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

Если список пуст, то возвращать пустой tuple().

ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
"""


def list_to_str(my_list, my_sep):
    if type(my_list) != list:
        return 'First arg must be list!'
    if type(my_sep) != str:
        return 'Second arg must be str!'
    if len(my_list) == 0:
        return tuple()
    else:
        new_list = my_sep.join(str(x) for x in my_list)
        return new_list, new_list.count(my_sep) ** 2


if __name__ == '__main__':
    print(list_to_str([1, "===", {5, 5, 6}, "hfkjhf"], "!"))
